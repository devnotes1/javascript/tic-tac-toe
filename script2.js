const result = document.querySelector('span');
const squares = [...document.querySelectorAll('.square')];
const btn = document.querySelector('button');
const firstPage = document.querySelector('.first-page');
const gameBoard = document.querySelector('.game-board');
const round = document.getElementById('round');
const contentPlayer1 = document.getElementById('player1');
const contentPlayer2 = document.getElementById('player2');

let roundNumber = 0;
let player1 = 0;
let player2 = 0;
let moveNumber = 0;

const classX = 'fa-solid fa-xmark';
const classO = 'fa-regular fa-circle';
let activePlayer = 'O';

let winningVariants = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 4, 8],
	[2, 4, 6],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
];

let fields = ['', '', '', '', '', '', '', '', ''];

squares.forEach(square =>
	square.addEventListener('click', e => {
		let { index } = e.target.dataset;
		if (fields[index] === '') {
			fields[index] = activePlayer;
			moveNumber++;
			if (activePlayer === 'O') {
				e.target.innerHTML = `<i class="${classO}"></i>`;
			} else if (activePlayer === 'X') {
				e.target.innerHTML = `<i class="${classX}"></i>`;
			}

			checkWinner();
			activePlayer = activePlayer === 'X' ? 'O' : 'X';
		}
	})
);

const init = () => {
	gameBoard.style.display = 'block';
	firstPage.style.display = 'none';
	if (roundNumber === 0) {
		round.textContent = ' ' + roundNumber;
		contentPlayer1.textContent = ' ' + player1;
		contentPlayer2.textContent = ' ' + player2;
	}
};

const showResult = () => {
	roundNumber++;
	round.textContent = ' ' + roundNumber;
	let winner;
	if (moveNumber <= 9) {
		if (activePlayer === 'O') {
			player1++;
			contentPlayer1.textContent = ' ' + player1;
			winner = 'wygrał player1';
		} else {
			player2++;
			contentPlayer2.textContent = ' ' + player2;
			winner = 'wygrał player2';
		}
	} else {
		winner = 'remis';
	}
	setTimeout(() => {
		alert(winner);
		reset();
	}, 1000);
};

const checkWinner = () => {
	for (let i = 0; i < winningVariants.length; i++) {
		const [indexA, indexB, indexC] = winningVariants[i];

		if (
			(fields[indexA] !== '' && fields[indexA] === fields[indexB] && fields[indexB] === fields[indexC]) ||
			moveNumber === 9
		)
			return showResult();
	}
};

const reset = () => {
	for (let square of squares) {
		square.innerHTML = '';
	}
	moveNumber = 0;
	fields = ['', '', '', '', '', '', '', '', ''];
	gameBoard.style.display = 'none';
	firstPage.style.display = 'flex';
};

btn.addEventListener('click', init);
